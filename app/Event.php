<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class Event extends Model
{
    use SoftDeletes;

    protected $table = 'events';
    protected $fillable = ['apis_id', 'description', 'start', 'end', 'all_day', 'color'];
    protected $casts = ['all_day' => 'boolean'];
    protected $appends = ['title', 'allDay'];
    protected $visible = ['id', 'allDay', 'title', 'start', 'end', 'color'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'start', 'end'];

    /**
     * @return boolean $isHolidayToday
     */
    public static function isHolidayToday()
    {
        return Cache::remember('holiday_today', (new Carbon())->endOfDay(), function () {
            return (boolean)self::holidays(null)->count();
        });
    }

    /**
     * @param null $date
     * @return static
     */
    public static function holidays($date = null)
    {
        /** @var Carbon $date */
        $date = self::parse($date);

        $holidays = self::getEvents($date->copy(), $date->copy())->filter(function ($event, $key) use ($date) {
            return ($date->between($event->start, $event->end)) && $event->holiday;
        });

        return $holidays;
    }

    /**
     * @param $date
     * @return $this|Carbon
     */
    private static function parse($date)
    {
        if (is_null($date)) {
            $date = new Carbon();
        } else if ($date instanceof \DateTime) {
            $date = (new Carbon())->setTimestamp($date->getTimestamp());
        }

        return $date;
    }

    /**
     *
     * Se repeat.end=null e occurrences=0 : evento infinito
     * holiday é um all_day
     */
    public static function apisOn($date = null)
    {
        $r = Collection::make();

        dd(self::getEvents($date, $date));

        foreach (self::getEvents($date, $date) as $item) {
            dd($item);
            foreach ($item->apis as $api) {
                $r->push($api);
            }
        }
        return $r->unique('id')->values();
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @return Collection
     */
    public static function getEvents(Carbon $start, Carbon $end)
    {
        $start = $start->copy()/*->startOfDay()*/;
        $end = $end->copy()/*->endOfDay()*/;

        $events = Event::with(['apis', 'repeat'])
            ->where('start', '<=', $end)
            ->get();

        /** @var Collection $replicatedEvents */
        $replicatedEvents = new Collection();

        foreach ($events as $key => $event) {

            //@Todo:Fixing fullcalendar rendering
            if ($event->all_day) {
                $event->end = $event->end->addDay()->startOfDay();
            }

            if (!is_null($event->repeat)) {
                $replicateds = null;
                //@Todo: dayEnd end null verify
                switch ($event->repeat->repeats_types_id) {
                    case RepeatType::DAILY:
                        $replicateds = self::daily($event, $start, $end);
                        break;
                    case RepeatType::WEEKLY:
                        $replicateds = self::weekly($event, $start, $end);
                        break;
                    case RepeatType::MONTHLY:
                        $replicateds = self::monthly($event, $start, $end);
                        break;
                    case RepeatType::YEARLY:
                        $replicateds = self::yearly($event, $start, $end);
                        break;
                }

                $replicatedEvents = $replicatedEvents->merge($replicateds);
            } else {
                $replicatedEvents->push($event->replicate());
            }

        }

        return $replicatedEvents;
    }

    /**
     * @param $event
     * @param $start
     * @param $end
     * @return Collection
     */
    private static function daily($event, $start, $end)
    {
        /** @var Carbon $dayStart */
        $dayStart = $event->start->copy();
        /** @var Carbon $dayEnd */
        $dayEnd = $event->end->copy();

        $replicatedEvents = new Collection();

        for (; $dayStart->lte($end); $dayStart = $dayStart->addDay(), $dayEnd = $dayEnd->addDay()) {
            if (!$dayStart->lt($start)) {
                $replicatedEvents->push(self::replicateEvent($event, $dayStart, $dayEnd));
            }
        }

        return $replicatedEvents;
    }

    /**
     * @param $event
     * @param $start
     * @param $end
     * @return mixed
     */
    private static function replicateEvent($event, $start, $end)
    {
        $e = $event->replicate();
        $e->id = $event->id;
        $e->start = $start->copy();
        $e->end = $end->copy();
        return $e;
    }

    /**
     * @param $event
     * @param $start
     * @param $end
     * @return Collection
     */
    private static function weekly($event, $start, $end)
    {
        /** @var Carbon $dayStart */
        $dayStart = $event->start->copy();
        /** @var Carbon $dayEnd */
        $dayEnd = $event->end->copy();

        $replicatedEvents = new Collection();

        for (; $dayStart->lte($end); $dayStart = $dayStart->addWeek(), $dayEnd = $dayEnd->addWeek()) {
            if (!$dayStart->lte($start)) {
                if (is_array($dows = $event->repeat->dows)) {
                    foreach ($dows as $dow) {
                        $dayStartDow = $dayStart->copy();
                        $dayEndDow = $dayEnd->copy();
                        if ($dayStartDow->dayOfWeek != $dow) {
                            $dayStartDow = $dayStartDow->next($dow);
                            $dayStartDow->setTime($dayStart->hour, $dayStart->minute, $dayStart->second);
                            $diff = $dayStart->diffInDays($dayStartDow);
                            $dayEndDow = $dayEndDow->addDays($diff);

                            if ($dayStartDow->gte($start) && $dayEndDow->lte($end)) {
                                $replicatedEvents->push(self::replicateEvent($event, $dayStartDow, $dayEndDow));
                            }
                        } else if ($dayStart->gte($start) && $dayEnd->lte($end)) {
                            $replicatedEvents->push(self::replicateEvent($event, $dayStart, $dayEnd));
                        }
                    }
                } else {
                    $replicatedEvents->push(self::replicateEvent($event, $dayStart, $dayEnd));
                }
            }
        }

        return $replicatedEvents;
    }

    /**
     * @param $event
     * @param $start
     * @param $end
     * @return Collection
     */
    private static function monthly($event, $start, $end)
    {

        /** @var Carbon $dayStart */
        $dayStart = $event->start->copy();
        /** @var Carbon $dayEnd */
        $dayEnd = $event->end->copy();

        $replicatedEvents = new Collection();

        //Calcing DOW just one time
        if ($event->repeat->dom == 0) {//DOW
            $dows = 0;
            $date = $dayStart->copy();
            for (; $date->month >= $dayStart->month; $date = $date->subWeek()) {
                $dows++;
            }
        }

        for (; $dayStart->lte($end); $dayStart = $dayStart->addMonth(), $dayEnd = $dayEnd->addMonth()) {
            if (!$dayStart->lt($start)) {
                if ($event->repeat->dom == 1) {//DOM
                    $replicatedEvents->push(self::replicateEvent($event, $dayStart, $dayEnd));
                } else if ($event->repeat->dom == 0) {//DOW
                    $domStart = $dayStart->copy();
                    $domStart->nthOfMonth($dows, $event->start->dayOfWeek);
                    $domStart->setTime($dayStart->hour, $dayStart->minute, $dayStart->second);

                    $domEnd = $dayEnd->copy();
                    $domEnd->nthOfMonth($dows, $event->end->dayOfWeek);
                    $domEnd->setTime($dayEnd->hour, $dayEnd->minute, $dayEnd->second);

                    $replicatedEvents->push(self::replicateEvent($event, $domStart, $domEnd));
                }
            }
        }

        return $replicatedEvents;
    }

    /**
     * @param $event
     * @param $start
     * @param $end
     * @return Collection
     */
    private static function yearly($event, $start, $end)
    {
        /** @var Carbon $dayStart */
        $dayStart = $event->start->copy();
        /** @var Carbon $dayEnd */
        $dayEnd = $event->end->copy();

        $replicatedEvents = new Collection();

        for (; $dayStart->lte($end); $dayStart = $dayStart->addYear(), $dayEnd = $dayEnd->addYear()) {
            if (!$dayStart->lt($start)) {
                $replicatedEvents->push(self::replicateEvent($event, $dayStart, $dayEnd));
            }
        }

        return $replicatedEvents;
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function apis()
    {
        return $this->belongsToMany(Api::class, 'apis_events', 'events_id', 'apis_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function repeat()
    {
        return $this->hasOne(Repeat::class, 'events_id');
    }

    /**
     * @return string
     */
    public function getTitleAttribute()
    {
        return $this->apis->implode('name', ', ');
    }

    /**
     * @return bool
     */
    public function getAllDayAttribute()
    {
        return (boolean)$this->attributes['all_day'] || (boolean)$this->attributes['holiday'];
    }

    /**
     * @param $value
     */
    public function setStartAttribute($value)
    {
        if (!is_object($value) && !empty($value) && is_string($value)) {
            $value = str_replace('/', '-', $value);
            $value = Carbon::createFromFormat('Y-m-d H:i:s', $value);
            $this->attributes['start'] = $value;
        } else if (!is_object($value)) {
            $this->attributes['start'] = null;
        }

        $this->attributes['start'] = $value;
    }

    /**
     * @param $value
     */
    public function setEndAttribute($value)
    {
        if (!is_object($value) && !empty($value) && is_string($value)) {
            $value = str_replace('/', '-', $value);
            $value = Carbon::createFromFormat('Y-m-d H:i:s', $value);
            $this->attributes['end'] = $value;
        } else if (!is_object($value)) {
            $this->attributes['end'] = null;
        }

        $this->attributes['end'] = $value;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

}
