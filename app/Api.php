<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    protected $table = 'apis';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
        'url',
        'key',
        'status'
    ];
    public function events()
    {
        return $this->belongsToMany(Event::class, 'apis_events', 'apis_id', 'events_id');
    }
}
