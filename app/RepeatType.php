<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RepeatType extends Model
{
    const DAILY = 1;
    const WEEKLY = 2;
    const MONTHLY = 3;
    const YEARLY = 4;

    protected $table = 'repeats_types';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'title'
    ];

    public static function types()
    {
        return [
            '1' => 'Diário',
            '2' => 'Semanal',
            '3' => 'Mensal',
            '4' => 'Anual'
        ];
    }

    public function repeats()
    {
        return $this->hasMany(Repeat::class, 'repeats_types_id');
    }
}
