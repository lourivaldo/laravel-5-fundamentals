<?php

namespace App;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Repeat extends Model
{
    const EVER = 1;
    const OCCURRENCES = 2;
    const DATE = 3;

    protected $table = 'repeats';
    protected $appends = ['end_type'];
    protected $casts = ['dows' => 'array'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'end'];
    protected $fillable = [
        'end',
        'occurrences',
        'dows',
        'dom',
        'repeats_types_id',
        'events_id',
        'ever'
    ];

    public function getEndTypeAttribute()
    {
        return !is_null($this->attributes['end']) ? REPEAT_TYPE_DATE : ($this->attributes['occurrences'] > 0 ? REPEAT_TYPE_OCCURRENCES : REPEAT_TYPE_EVER);
//        return $this->attributes['ever'] ? REPEAT_TYPE_EVER : (!is_null($this->attributes['end']) ? REPEAT_TYPE_DATE : ($this->attributes['occurrences'] > 0 ? REPEAT_TYPE_OCCURRENCES : REPEAT_TYPE_EVER));

    }

    public function setEndAttribute($value)
    {
        return empty($value) ? null : Carbon::createFromFormat('Y-m-d', $value);

    }

    public function repeatType()
    {
        return $this->hasOne(RepeatType::class, 'id', 'repeats_types_id');
    }

    public function event()
    {
        return $this->belongsTo(Event::class, 'events_id');
    }

}
