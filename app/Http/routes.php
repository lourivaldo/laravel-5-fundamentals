<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/external-resource', 'ExternalController@index');
Route::get('/external-resource/external', 'ExternalController@external');
Route::get('/external-resource/test', 'ExternalController@test');

Route::any('/chart', 'ChartController@index');
Route::any('/chart/get', 'ChartController@getChart');
Route::any('/chart/get2', 'ChartController@getChart2');
Route::resource('/calendar', 'CalendarController');
//Route::resource('/user', 'UserController');
Route::get('/events', 'CalendarController@calendar');
Route::get('/choose', 'CalendarController@choose');

Route::resource('/manager/calendar', 'CalendarController', ['except' => 'show']);
Route::get('/manager/calendar/lists', 'CalendarController@lists');
Route::get('/manager/calendar/choose', 'CalendarController@choose');
Route::any('/manager/user/paginate', 'UserController@paginate');
Route::get('/manager/user', 'UserController@index');

Route::get('/test', function (\Illuminate\Http\Request $request) {

    $type = function ($o) {
        return $o['type'] == 1;
    };

    $limit = function ($o) {
        return $o['limit'] <= 5;
    };

    $expiration = function ($o) {
        return $o['expiration'] == null || $o['expiration'] <= 10;
    };

    $collection = [
        ['limit' => 10, 'type' => 3, 'expiration' => 10],
        ['limit' => 1, 'type' => 1, 'expiration' => 30],
        ['limit' => 5, 'type' => 2, 'expiration' => 7],
        ['limit' => 9, 'type' => 1, 'expiration' => 2],
        ['limit' => 1, 'type' => 1, 'expiration' => null],
        ['limit' => 1, 'type' => 1, 'expiration' => 0],
        ['limit' => 7, 'type' => 1, 'expiration' => 0],
    ];

    $mostProx = function ($arr, $limit) {

        if (count($arr) == 0) return;

        usort($arr, function ($a, $b) {
            return $a['limit'] > $b['limit'];
        });

        $c = $arr[0];
        for ($i = 0; $i < count($arr); $i++) {
            if ($arr[$i]['limit'] <= $limit) {
                $c = $arr[$i];
                continue;
            }

            return $c;
        }
    };

    dd($mostProx($collection, 6));
    dd($collection);

    $filters = [$type, $limit, $expiration];

    foreach ($filters as $filter) {

//        if (count($collection) <= 1) break;

        $collection = array_filter($collection, $filter);
        print_r($collection);
        echo "<br>";
    }

    dd($collection);

    return $request->all();
});

Route::put('/test/{id}', function (\Illuminate\Http\Request $request) {
//    return new \App\User(['name'=>'Loro']);
    return $request->all();
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
