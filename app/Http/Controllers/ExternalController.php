<?php
/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 23/04/2016
 * Time: 09:08
 */

namespace App\Http\Controllers;

use App\Http\Requests\CreateEventRequest;
use App\Http\Requests\ListsEventRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ExternalController extends Controller
{

    public function index()
    {
        return view('external.index');
    }

    public function external()
    {
        $start = new Carbon();
        $data = [];

        for ($i = 1000; $i > 0; $i--) {
            $data[] = [
                'index' => $i,
                'msg' => str_random(1000),
                'data' => new Carbon()
            ];
        }

        for ($i = 1000; $i > 0; $i--) {
            $data[] = [
                'index' => $i,
                'msg' => str_random(1000),
                'data' => new Carbon()
            ];
        }

        for ($i = 1000; $i > 0; $i--) {
            $data[] = [
                'index' => $i,
                'msg' => str_random(1000),
                'data' => new Carbon()
            ];
        }

        for ($i = 1000; $i > 0; $i--) {
            $data[] = [
                'index' => $i,
                'msg' => str_random(1000),
                'data' => new Carbon()
            ];
        }
        sleep(10);

        for ($i = 1000; $i > 0; $i--) {
            $data[] = [
                'index' => $i,
                'msg' => str_random(1000),
                'data' => new Carbon()
            ];
        }

        for ($i = 1000; $i > 0; $i--) {
            $data[] = [
                'index' => $i,
                'msg' => str_random(1000),
                'data' => new Carbon()
            ];
        }

        for ($i = 1000; $i > 0; $i--) {
            $data[] = [
                'index' => $i,
                'msg' => str_random(1000),
                'data' => new Carbon()
            ];
        }

        for ($i = 1000; $i > 0; $i--) {
            $data[] = [
                'index' => $i,
                'msg' => str_random(1000),
                'data' => new Carbon()
            ];
        }

        return ['start' => $start, 'end' => new Carbon()];
    }

    public function test()
    {
        $start = new Carbon();

        $client = new \GuzzleHttp\Client();

        $res = $client->request('GET', 'http://localhost/laravel-5-fundamentals/public/external-resource/external', [
            'auth' => ['user', 'pass']
        ]);

        $data = [
            'body' => $res->getBody(),
            'code' => $res->getStatusCode(),
            'type' => $res->getHeaderLine('content-type'),
        ];

        return ['diff' => $start->diffInSeconds(new Carbon()), 'data' => $data];
    }

}