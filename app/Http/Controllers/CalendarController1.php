<?php
/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 23/04/2016
 * Time: 09:08
 */

namespace App\Http\Controllers;

use App\Api;
use App\Event;
use App\Http\Requests\CreateEventRequest;
use App\Http\Requests\ListsEventRequest;
use App\RepeatType;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CalendarController1 extends Controller
{
    public function index()
    {
        return view('calendar.index');
    }

    public function create()
    {
        $apis = Api::all();
        return view('calendar.create', compact(['apis']));
    }

    public function store(CreateEventRequest $request, Event $event)
    {
        if ($event = $event->create($request->all())) {
            return ($event->apis()->sync($request->input('apis')));
        }
    }

    public function edit($id)
    {
        /** @var Event $event */
        $event = Event::findOrFail($id);
        $apis = Api::all();
        return view('calendar.edit', compact(['event', 'apis']));
    }

    public function update(Request $request, $id)
    {
        $event = Event::findOrFail($id);
        if ($event->update($request->all())) {
            $event->apis()->sync($request->get('apis'));
        }
    }

    public function calendar(ListsEventRequest $request)
    {
        $start = $request->get('start');
        $start = Carbon::createFromFormat("Y-m-d", $start);
        $start->startOfDay();
        $end = $request->get('end');
        $end = Carbon::createFromFormat("Y-m-d", $end);
        $end->endOfDay();
        return Event::getFullcalendarFormat($start, $end);
    }

    //Api ativa > Feriado > Horario > Usuario > Agencia
    //Api ativa > !Horario
    //Api ativa > !Usuario
    //Api ativa > !Agencia
    public function choose()
    {
//        return(Event::with('repeats.repeatsTypes')->);

        /** @var Api $activeApis */
        $activeApis = /*Cache::remember('', 1, function () {
            return */
            Api::select('status')->where('status', '=', 1)->get();
        /*});*/

        if ($activeApis->count() > 1) {
            //1Checar Feriado - usar apenas definidas
            //1.1 se apenas uma
            //1.1.1 busca por ela
            //1.2 se nao
            //2 busca pelo horario
            $data = Event::on();
//            dd($data->toArray());


//            dd(Event::isHolidayToday());
//            dd(Event::isHoliday());

            return (Event::apisOn((new Carbon())->setTime(0, 0, 0))->toArray());
//            return(Event::eventsOn((new Carbon())->setTime(0, 0, 0))->toArray());
//            return Event::on();

        } else if ($activeApis->count() == 1) {
            //Faz consulta com a unica disponivel
            dd($activeApis->first());
        } else {
            //Nao faz consulta
        }
    }
}