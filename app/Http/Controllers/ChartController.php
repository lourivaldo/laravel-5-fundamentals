<?php
/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 23/04/2016
 * Time: 09:08
 */

namespace App\Http\Controllers;

use App\Search;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function index()
    {
        return view('charts.index');
    }

    public function getChart(Request $request)
    {
        $start = null;
        $end = null;

        if ($request->has("start")) {
            $timestamp = (int)$request->get("start");
            $start = new \DateTime();
            $start->setTimestamp($timestamp)->setTime(0, 0);
        }

        if ($request->has("end")) {
            $timestamp = (int)$request->get("end");
            $end = new \DateTime();
            $end->setTimestamp($timestamp)->setTime(0, 0);
        }

        //Chart format return
        $data = Search::groupedByApisId(null, $start, $end);

        return $data;
    }

    public function getChart2(Request $request)
    {
        $start = null;
        $end = null;

        if ($request->has("start")) {
            $timestamp = (int)$request->get("start");
            $start = new \DateTime();
            $start->setTimestamp($timestamp)->setTime(0, 0);
        }

        if ($request->has("end")) {
            $timestamp = (int)$request->get("end");
            $end = new \DateTime();
            $end->setTimestamp($timestamp)->setTime(0, 0);
        }

        //Chart format return
        $data = Search::getChat2(null, $start, $end);

        return $data;
    }




}