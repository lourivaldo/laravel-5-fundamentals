<?php
/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 23/04/2016
 * Time: 09:08
 */

namespace App\Http\Controllers;

use App\Http\Requests\CreateEventRequest;
use App\Http\Requests\ListsEventRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{

    public function index()
    {
        return view('user.index');
    }

    public function paginate()
    {
        return Datatables::of(User::all())->make(true);
    }

}