<?php
/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 23/04/2016
 * Time: 09:08
 */

namespace App\Http\Controllers;

use App\Http\Requests\CreateEventRequest;
use App\Http\Requests\ListsEventRequest;
use App\Api;
use App\Event;
use App\RepeatType;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CalendarController extends Controller
{

    private $js = ['requireJS' => ['calendar/index.js']];

    public function index()
    {
        return view('calendar.index');
    }

    public function create()
    {
        $repeatsTypes = RepeatType::types();
        $apis = Api::all();

        return view('calendar.create', compact(['apis', 'repeatsTypes']), ['requireJS' => ['calendar/create.js']]);
    }

    public function store(CreateEventRequest $request, Event $event)
    {
        if ($event = $event->create($request->all())) {
            $event->apis()->sync($request->input('apis'));
        }

        return redirect("/manager/calendar");
    }

    public function edit($id)
    {
        /** @var Event $event */
        $data = Event::with('repeat')->findOrFail($id);

        $repeatsTypes = RepeatType::types();
        $apis = Api::all();
        return view('calendar.edit', compact(['data', 'apis', 'repeatsTypes']), ['requireJS' => ['calendar/create.js']]);
    }

    public function update(Request $request, $id)
    {
        $event = Event::with('repeat')->findOrFail($id);
//        dd($request->all());
        if ($event->update($request->all())) {
            $event->apis()->sync($request->get('apis'));
            if ($request->has('repeatable') && $request->has('repeat')) {
                //@Todo: tirar daqui
                $ever = $request->input('repeat.end_type') == REPEAT_TYPE_EVER ? 1 : 0;
                $event->repeat()->updateOrCreate([], array_merge($request->get('repeat'), ['ever' => $ever]));
            } else if ($event->has('repeat')) {
                $event->repeat()->forceDelete();
            }
            return redirect('/manager/calendar');
        } else {
            return back()->withErrors();
        }
    }

    public function lists(ListsEventRequest $request)
    {
//        return Event::holidays();
        $start = $request->get('start');
        $end = $request->get('end');
        $start = Carbon::createFromFormat("Y-m-d", $start);
        $end = Carbon::createFromFormat("Y-m-d", $end);
        return Event::getEvents($start, $end);
    }

    //Api ativa > Feriado > Horario > Usuario > Agencia
    //Api ativa > !Horario
    //Api ativa > !Usuario
    //Api ativa > !Agencia
    public function choose()
    {
        /** @var Api $activeApis */
        $activeApis = /*Cache::remember('', 1, function () {
            return */
            Api::select('status')->where('status', '=', 1)->get();
        /*});*/

        if ($activeApis->count() > 1) {
            //1Checar Feriado - usar apenas definidas
            //1.1 se apenas uma
            //1.1.1 busca por ela
            //1.2 se nao
            //2 busca pelo horario

            $date = new Carbon();
            $events = Event::apisOn($date)->toArray();

            return $events;

        } else if ($activeApis->count() == 1) {
            //Faz consulta com a unica disponivel
            dd($activeApis->first());
        } else {
            //Nao faz consulta
        }
    }
}