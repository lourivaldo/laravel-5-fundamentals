<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        /**
         * @const REPEAT_TYPE_EVER
         */
        defined('REPEAT_TYPE_EVER') or define('REPEAT_TYPE_EVER', 1);
        /**
         * @const REPEAT_TYPE_OCCURRENCES
         */
        defined('REPEAT_TYPE_OCCURRENCES') or define('REPEAT_TYPE_OCCURRENCES', 2);
        /**
         * @const REPEAT_TYPE_DATE
         */
        defined('REPEAT_TYPE_DATE') or define('REPEAT_TYPE_DATE', 3);
    }
}
