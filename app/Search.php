<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Search extends Model
{
    protected $table = "searchs";

    /**
     * @param array $ids
     * @param \DateTime|null $dateStart
     * @param \DateTime|null $dateEnd
     * @return array
     */
    public static function groupedByApisId($ids = null, \DateTime $dateStart = null, \DateTime $dateEnd = null)
    {
        //Getting apis id from searches (including nulls)
        if (is_null($ids)) {
            $ids = self::getDistinctApi()->pluck('apis_id');
        }

        //Date intervals
        if (is_null($dateStart)) {
            $dateStart = new \DateTime(self::min("created_at"));
            $dateStart->setTime(0, 0, 0); //First Date
        }

        if (is_null($dateEnd)) {
            $dateEnd = new \DateTime(self::max("created_at"));
            $dateEnd->setTime(23, 59, 59); //Last Date
        }

        $searchesByApi = [];
        $searchesByApi["total"] = 0;
        $searchesByApi["apis"] = self::getDistinctApi()->lists('api.name', 'apis_id');
        $searchesByApi["date"] = (new \DateTime())->getTimestamp();

        foreach ($ids as $name => $id) {

            /** @var Collection $searches */
            $searches = /*Cache::remember('searches_' . $id, 10, function () use ($id, $dateStart, $dateEnd) {
                return
                    */
                self::select(['created_at', DB::raw('COUNT(created_at) as count')])
                    ->groupBy([DB::raw('YEAR(created_at)'), DB::raw('MONTH(created_at)'), DB::raw('DAY(created_at)')])
                    ->where('apis_id', '=', $id)
                    ->whereBetween('created_at', [$dateStart, $dateEnd])
                    ->orderBy('created_at')
                    ->get();
            /* });*/

            $date1 = clone $dateStart;
            $searchByApi = [];
            for ($i = 0; $date1 <= $dateEnd; $date1->modify("+1 day")) {
                if (isset($searches[$i]) &&
                    ((int)$date1->diff($searches[$i]->created_at->setTime(0, 0))->format("%d")
                        + (int)$date1->diff($searches[$i]->created_at->setTime(0, 0))->format("%m")
                        + (int)$date1->diff($searches[$i]->created_at->setTime(0, 0))->format("%y")) == 0
                ) {
                    $searchesByApi["total"] += $searches[$i]->count;
                    $searchByApi[] = [
                        $searches[$i]->created_at->setTime(0, 0, 0)->timestamp * 1000,
                        $searches[$i]->count
                    ];
                    $i++;
                } else {
                    $searchByApi[] = [
                        $date1->setTime(0, 0, 0)->getTimestamp() * 1000,
                        0
                    ];
                }
            }

            //Grouping by api type
            $searchesByApi["items"][$id] = $searchByApi;
        }

        return $searchesByApi;
    }

    /**
     * @return array
     */
    public static function getDistinctApi()
    {
        $apis = Search::with('api')
            ->select(['apis_id'])
            ->distinct()
//            ->orderBy('apis_id')
            ->get();

        return $apis;
    }

    /**
     * @return array
     */
    public static function getChat2($apis = null, \DateTime $dateStart = null, \DateTime $dateEnd = null)
    {
        $data = [];

        //Getting apis id from searches (including nulls)
        if (is_null($apis)) {
            $apis = self::getDistinctApi();
        }

        //Date intervals
        if (is_null($dateStart)) {
            $dateStart = new \DateTime(self::min("created_at"));
            $dateStart->setTime(0, 0, 0); //First Date
        }

        if (is_null($dateEnd)) {
            $dateEnd = new \DateTime(self::max("created_at"));
            $dateEnd->setTime(23, 59, 59); //Last Date
        }

        foreach ($apis as $name => $api) {

            $results = self::select([DB::raw('HOUR(created_at) as hour'), DB::raw('COUNT(created_at) as count')])
                ->groupBy([DB::raw('HOUR(created_at)')])
                ->where('apis_id', '=', $api->apis_id)
                ->whereBetween('created_at', [$dateStart, $dateEnd])
                ->get();

            $d = [];
            for ($i = 0; $i < 24; $i++) {
                if ($r = $results->where('hour', $i)->first()) {
                    $d[] = $r->count;
                } else {
                    $d[] = 0;
                }
            }

            $o = new \stdClass();
            $o->name = isset($api->api->name) ? $api->api->name : "Indefinido";
            $o->data = $d;

            $data[] = $o;
        }


        return $data;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function api()
    {
        return $this->hasOne(Api::class, 'id', 'apis_id');
    }

}
