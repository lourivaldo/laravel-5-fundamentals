$(document).ready(function () {
    $('#calendar').fullCalendar({
        dayRender: function (date, cell) {
            if (date < moment()) {
                $(cell).addClass('disabled');
                $(cell).css('background-color', '#ff9f89');
            }
        },
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        lang: 'pt-br',
        buttonIcons: false, // show the prev/next text
        businessHours: true, // display business hours
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        //events: data
        selectable: true,
        selectHelper: true,
        select: function (start, end) {
            console.log(start);
            console.log(end);
            var title = prompt('Event Title:');
            var eventData;
            if (title) {
                eventData = {
                    title: title,
                    start: start,
                    end: end
                };
                $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
            }
            $('#calendar').fullCalendar('unselect');
        },

        events: {

            nextDayThreshold: 9,
            url: 'calendar/lists',
            success: function (data) {
                $.each(data, function (i, item) {
                    item.url = 'calendar/' + item.id + '/edit';
                });
                console.log('Success');
            },
            error: function () {
                //$('#script-warning').show();
                console.log('Error');
            }
        },
        loading: function (bool) {
            $('#loading').toggle(bool);
            console.log('Loading');
        }
    });
});
