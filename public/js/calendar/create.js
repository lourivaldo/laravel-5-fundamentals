$(document).ready(function () {

    $('#repeat-type').change(repeat);
    repeat();

    //$('#start').on('change keyup keypress blur', disable);

    $('.datetime-picker').datetimepicker({
        //format: 'LT',
        //locale:'en',
        minDate: 'now'
    });

    /*$('#form-add-avent').ajaxForm({
        beforeSubmit: function (arr, $form, options) {
            var start = null;
            var end = null;
            try {
                start = moment($form.find('#start').val());
                end = moment($form.find('#start').val());
            } catch (e) {
                console.log("Moment Error");
                return false;
            }

            if (start.isAfter(end)) {
                console.log("Start after end");
                return false;
            }
        }
    });*/
});

function repeat() {
    $("[id^=repeat-target-]").hide();
    $("#repeat-target-" + $('#repeat-type').val()).show();
}

function disable(){
    var start = $('#start').val();

    try {
        date = moment(start);
        weekday = date.weekday();

        $('[id^=dows-]').prop('disabled', false);
        $('[id^=dows-]').prop('checked', false);
        $('#dows-' + weekday).prop('checked', true);
        $('#dows-' + weekday).prop('disabled', true);
        for (var i = weekday - 1; i >= 0; i--) {
            $('#dows-' + i).prop('checked', false);
            $('#dows-' + i).prop('disabled', true);
        }
    } catch (e) {
        console.log("Erro");
    }
}


