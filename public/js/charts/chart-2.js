$.getJSON('/chart/get2', function (data) {

    $('#container-chart-2').highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: 'Buscas por hora',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: [
                '00:00',
                '01:00',
                '02:00',
                '03:00',
                '04:00',
                '05:00',
                '06:00',
                '07:00',
                '08:00',
                '09:00',
                '10:00',
                '11:00',
                '12:00',
                '13:00',
                '14:00',
                '15:00',
                '16:00',
                '17:00',
                '18:00',
                '19:00',
                '20:00',
                '21:00',
                '22:00',
                '23:00'
            ]
        },
        yAxis: {
            title: {
                text: 'Qtd. de buscas'
            },
            /*
             plotLines: [{
             value: 0,
             width: 1,
             color: '#808080'
             }]*/
        },
        tooltip: {
            valueSuffix: ' buscas'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        plotOptions: {
            spline: {
                lineWidth: 3,
                states: {
                    hover: {
                        lineWidth: 4
                    }
                },
                marker: {
                    enabled: false,
                    radius: 3
                },
            }
        },
        series: data
    });
});
