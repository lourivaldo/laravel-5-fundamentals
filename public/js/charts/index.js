//$.getJSON('/chart/get?start=1398297600&end=1401069900', function (data) {
$.getJSON('/chart/get', function (data) {
    var seriesOptions = [];

    $.each(data.items, function (i, item) {
        var name = data.apis[i];
        name = name ? name : "Indefinido";

        seriesOptions.push({
            name: name,
            data: item
        });
    });

    $('#container-chart').highcharts('StockChart', {
        title: {
            text: 'Buscas Por API' + " (Total de " + data.total + " buscas)"
        },
        subtitle: {
            text: document.ontouchstart === undefined ?
                'Clique e arraste na área do desenho para fazer zoom' :
                'Comprima o gráfico para aumentar o zoom'
        },
        rangeSelector: {
            selected: 0,
            inputDateFormat: "%e %b %Y"
        },
        chart: {
            type: 'spline',
            zoomType: 'x',
            events: {
                load: function () {

                }
            }
        },
        xAxis: {
            ordinal: false,
            events: {
                afterSetExtremes: function (e) {
                    var sums = [], total = 0, chart = this;
                    //console.log(chart.series[0]);
                    for (var s in chart.series) {
                        sums[s] = 0;
                        for (var i in chart.series[s].data) {
                            point = chart.series[s].data[i];
                            if (point.x >= chart.min && point.x <= chart.max)
                                sums[s] += point.y;
                        }
                    }

                    $('#api-sum-selected').html("");
                    for (var sum in sums) {
                        color = chart.series[sum].color;
                        name = chart.series[sum].name;
                        total += sums[sum];
                        $('#api-sum-selected').append('<li><span style="color:' + color + '">' + name + "</span>: " + sums[sum] + '</li>');
                    }
                    $('#api-sum-selected').append('<li>Total: ' + total + '</li>');

                }
            }
        },
        yAxis: [{
            title: {
                text: "Qtde. de Buscas",
                x: 5
            },
            labels: {
                style: {"fontWeight": "bold"},
                x: 5
            },
            min: 0,
            allowDecimals: false
        }],
        tooltip: {
            headerFormat: '<span style="font-weight: bold;font-size: 13px">{point.x:%a, %d %b %y}</span><br/><br/>',
            pointFormat: '<br/><span style="color:{series.color};font-weight: bold;">' +
            '{series.name}</span>:<span style="font-weight: bold">{point.y:.0f} buscas</span><br/>'
        },
        legend: {
            enabled: true
        },
        plotOptions: {
            series: {
                allowPointSelect: true
            },
            spline: {
                lineWidth: 3,
                states: {
                    hover: {
                        lineWidth: 4
                    }
                },
                marker: {
                    enabled: false,
                    radius: 3
                },
            }
        },
        series: seriesOptions
    });
});
