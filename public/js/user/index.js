angular.module('laravelApp', ['datatables']).controller('WithAjaxCtrl', WithAjaxCtrl);

function WithAjaxCtrl(DTOptionsBuilder, DTColumnBuilder) {
    var vm = this;

    vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('ajax', {
            // dataSrc: 'data',
            url: '/manager/user/paginate',
            type: 'POST'
        })
        // or here
        .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers');

    vm.dtColumns = [
        DTColumnBuilder.newColumn('id').withTitle('ID'),
        DTColumnBuilder.newColumn('fullname').withTitle('First name'),
        DTColumnBuilder.newColumn('email').withTitle('Last name')/*.notVisible()*/
    ];
}