<?php
use Phinx\Seed\AbstractSeed;

/**
 * OrdersStatusPostecipado seed.
 */
class FakeDatabaseSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        /** @var \App\Model\Table\ProvidersTable Providers */
        $this->Providers = \Cake\ORM\TableRegistry::get('Providers');

        $deleteProvidersIds = [11130, 8931, 8953, 936, 5966, 8718, 564, 5763, 8993, 3236, 331, 9227,
            9227, 1729, 10363, 7416, 8160, 1022, 1464, 5096, 8746, 8057, 4271, 8026];

        $query = $this->Providers->find();

        $query->contain([
            'Fidelities',
            'ParentProviders',
            'ProvidersFiles',
            'Notes',
            'Quotations'
        ]);

        $providers = $query->skip;


        foreach ($providers as $provider) {
			


        }
    }
}
