<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('events', function (Blueprint $table) {
            $table->integer('id', true);

            $table->text('description')->nullable();
            $table->boolean('all_day')->default(0);
            $table->boolean('holiday')->default(0);

            $table->timestamp('start');
            $table->timestamp('end')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('apis_events', function (Blueprint $table) {
            $table->integer('apis_id');
            $table->integer('events_id');

            $table->foreign('apis_id')->references('id')->on('apis');
            $table->foreign('events_id')->references('id')->on('events');

            $table->primary(['apis_id', 'events_id']);
        });

        Schema::create('repeats_types', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('title', 225);

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('repeats', function (Blueprint $table) {
            $table->integer('id', true);
            $table->date('end')->nullable();
            $table->unsignedInteger('occurrences')->default(0);
            $table->string('dows')->nullable();
            $table->boolean('dom')->default(1);//1 - Dia do mes/ 2 - Dia da semana

            $table->boolean('ever')->default(1);

            $table->integer('repeats_types_id');
            $table->foreign('repeats_types_id')->references('id')->on('repeats_types');

            $table->integer('events_id')->nullable();
            $table->foreign('events_id')->references('id')->on('events');

            $table->timestamps();
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
        Schema::drop('repeats');
        Schema::drop('repeats_types');
    }
}
