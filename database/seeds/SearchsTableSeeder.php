<?php
namespace database\seeds;

use Illuminate\Database\Seeder;
use Faker;
use Illuminate\Support\Facades\DB;

class SearchsTableSeeder extends Seeder
{

    public function run()
    {
//        DB::table('searchs')->delete();

        $faker = Faker\Factory::create();
        $date = new \DateTime();
//        $date->modify("-24 months");
        $date->modify("+4 years");

        for ($i = 0; $i < 100; ++$i) {
            $search = new \App\Search();
            $search->origin = str_random(4);
            $search->destination = str_random(4);
            $search->type_trip = $faker->numberBetween(0, 1);
            $search->adults = $faker->numberBetween(1, 10);
            $search->children = $faker->numberBetween(0, 2);
            $search->users_id = $faker->numberBetween(1, 49);
            $search->apis_id = $faker->numberBetween(1, 3);

            if($faker->numberBetween(0,100) <= 10){
                $date->modify("+1 days");
            }

            if($i == 1000 || $i == 3200){
                $date->modify("+". $faker->numberBetween(1, 2)." months");
            }

            $search->created_at = $date->modify("+" . $faker->numberBetween(1, 59) . " minutes");
            $search->updated_at = $date;

            $search->save();
        }
    }
}
