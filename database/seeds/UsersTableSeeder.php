<?php
namespace database\seeds;

use Illuminate\Database\Seeder;
use Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();

        /** @var $faker Faker\Generator */
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 50; ++$i) {
            $user = new \App\User();
            $user->fullname = $faker->name;
            $user->email = $faker->unique()->email;
            $user->password = Hash::make('secret');
            $user->gender = $faker->randomElement(['M','F']);
            $user->save();
        }
    }
}
