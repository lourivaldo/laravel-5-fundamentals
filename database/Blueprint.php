<?php
/**
 * Created by PhpStorm.
 * User: Lourivaldo
 * Date: 21/04/2016
 * Time: 13:47
 */

namespace database;


class Blueprint extends \Illuminate\Database\Schema\Blueprint
{
    protected function createIndexName($type, array $columns)
    {
        $index = strtolower($this->table.'_'.str_random(3).'_'.$type);
        return str_replace(array('-', '.'), '_', $index);
    }
}