<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payments', function(Blueprint $table)
		{
			$table->integer('id');
			$table->integer('ops_id');
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('payments_types_id')->index('fk_payments_payments_types1_idx');
			$table->primary(['id','ops_id']);
			$table->index('ops_id','fk_payments_ops1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments');
	}

}
