<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToConfigsUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('configs_users', function(Blueprint $table)
		{
			$table->foreign('configs_id', 'fk_configs_has_users_configs1')->references('id')->on('configs')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('users_id', 'fk_configs_has_users_users1')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('configs_users', function(Blueprint $table)
		{
			$table->dropForeign('fk_configs_has_users_configs1');
			$table->dropForeign('fk_configs_has_users_users1');
		});
	}

}
