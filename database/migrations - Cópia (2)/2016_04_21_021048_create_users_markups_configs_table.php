<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersMarkupsConfigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_markups_configs', function(Blueprint $table)
		{
			$table->integer('markups_configs_id')->index('fk_users_has_markups_configs_markups_configs1_idx');
			$table->integer('users_id')->index('fk_users_has_markups_configs_users1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_markups_configs');
	}

}
