<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToOpsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ops', function(Blueprint $table)
		{
			$table->foreign('quotations_id', 'fk_ops_quotations1')->references('id')->on('quotations')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ops', function(Blueprint $table)
		{
			$table->dropForeign('fk_ops_quotations1');
		});
	}

}
