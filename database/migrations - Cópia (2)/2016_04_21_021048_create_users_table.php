<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('fullname', 225);
			$table->string('email', 60)->unique('email_UNIQUE');
			$table->string('password', 225);
			$table->char('gender', 1);
			$table->string('remember_token', 225)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(0);
			$table->integer('roles_id')->index('fk_users_roles_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
