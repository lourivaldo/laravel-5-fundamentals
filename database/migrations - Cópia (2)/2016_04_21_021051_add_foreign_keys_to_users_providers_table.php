<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_providers', function(Blueprint $table)
		{
			$table->foreign('providers_id', 'fk_users_has_providers_providers1')->references('id')->on('providers')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('users_id', 'fk_users_has_providers_users1')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_providers', function(Blueprint $table)
		{
			$table->dropForeign('fk_users_has_providers_providers1');
			$table->dropForeign('fk_users_has_providers_users1');
		});
	}

}
