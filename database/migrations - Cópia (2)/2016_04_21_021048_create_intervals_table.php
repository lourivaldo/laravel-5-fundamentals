<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIntervalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('intervals', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->float('miles_start', 10, 0);
			$table->float('miles_end', 10, 0);
			$table->float('interval', 10, 0);
			$table->float('price', 10, 0);
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('markups_id')->index('fk_intervals_markups1_idx');
			$table->index(['miles_start','miles_end','markups_id'], 'abitrary_unique_miles');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('intervals');
	}

}
