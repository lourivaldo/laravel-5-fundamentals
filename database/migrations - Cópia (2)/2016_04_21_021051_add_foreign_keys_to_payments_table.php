<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('payments', function(Blueprint $table)
		{
			$table->foreign('ops_id', 'fk_payments_ops1')->references('id')->on('ops')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('payments_types_id', 'fk_payments_payments_types1')->references('id')->on('payments_types')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('payments', function(Blueprint $table)
		{
			$table->dropForeign('fk_payments_ops1');
			$table->dropForeign('fk_payments_payments_types1');
		});
	}

}
