<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('miles', function(Blueprint $table)
		{
			$table->foreign('quotations_id', 'fk_miles_quotations1')->references('id')->on('quotations')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('miles', function(Blueprint $table)
		{
			$table->dropForeign('fk_miles_quotations1');
		});
	}

}
