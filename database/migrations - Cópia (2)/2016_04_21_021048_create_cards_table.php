<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cards', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->float('miles', 10, 0);
			$table->integer('code');
			$table->string('username', 225);
			$table->string('password', 225);
			$table->string('additional_password', 225)->nullable();
			$table->date('expires');
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('companies_id')->index('fk_cards_companies1_idx');
			$table->integer('providers_id')->index('fk_cards_providers1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cards');
	}

}
