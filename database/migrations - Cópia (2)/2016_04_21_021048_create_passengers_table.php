<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePassengersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('passengers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('fullname_passenger', 150);
			$table->char('gender_passenger', 1);
			$table->string('email_passenger', 125)->nullable();
			$table->date('birthday_passenger')->nullable();
			$table->string('cpf_passenger', 16)->nullable();
			$table->bigInteger('rg_passenger')->nullable();
			$table->boolean('code_passenger')->nullable();
			$table->bigInteger('phone_passenger')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('passengers');
	}

}
