<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCounterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('counter', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('configs_id');
			$table->integer('users_id');
			$table->integer('quantity')->default(1);
			$table->timestamps();
			$table->unique(['configs_id','users_id','id'], 'COMPOSE_KEY');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('counter');
	}

}
