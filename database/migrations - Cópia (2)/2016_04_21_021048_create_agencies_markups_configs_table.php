<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgenciesMarkupsConfigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('agencies_markups_configs', function(Blueprint $table)
		{
			$table->integer('agencies_id')->index('fk_agencies_has_markups_configs_agencies1_idx');
			$table->integer('markups_configs_id')->index('fk_agencies_has_markups_configs_markups_configs1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('agencies_markups_configs');
	}

}
