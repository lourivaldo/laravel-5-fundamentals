<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rates', function(Blueprint $table)
		{
			$table->integer('id');
			$table->boolean('group');
			$table->float('value', 10, 0);
			$table->float('atareo', 10, 0);
			$table->boolean('category');
			$table->float('connection', 10, 0)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('airports_id');
			$table->primary(['id','airports_id']);
			$table->index('airports_id','fk_tax_airports1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rates');
	}

}
