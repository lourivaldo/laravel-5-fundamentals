<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToConfigsRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('configs_roles', function(Blueprint $table)
		{
			$table->foreign('configs_id', 'fk_configs_has_roles_configs1')->references('id')->on('configs')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('roles_id', 'fk_configs_has_roles_roles1')->references('id')->on('roles')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('configs_roles', function(Blueprint $table)
		{
			$table->dropForeign('fk_configs_has_roles_configs1');
			$table->dropForeign('fk_configs_has_roles_roles1');
		});
	}

}
