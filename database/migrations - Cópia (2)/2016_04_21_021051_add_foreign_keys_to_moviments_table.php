<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMovimentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('moviments', function(Blueprint $table)
		{
			$table->foreign('cards_id', 'fk_moviments_cards1')->references('id')->on('cards')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('stocks_id', 'fk_moviments_stocks1')->references('id')->on('stocks')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('moviments', function(Blueprint $table)
		{
			$table->dropForeign('fk_moviments_cards1');
			$table->dropForeign('fk_moviments_stocks1');
		});
	}

}
