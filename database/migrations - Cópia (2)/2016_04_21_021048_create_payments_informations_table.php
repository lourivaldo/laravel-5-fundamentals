<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentsInformationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payments_informations', function(Blueprint $table)
		{
			$table->integer('id');
			$table->integer('payments_id');
			$table->string('barcode', 300)->nullable();
			$table->string('files_path', 125)->nullable();
			$table->string('token', 300)->nullable();
			$table->date('payment_date')->nullable();
			$table->float('amount_paid', 10, 0)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->primary(['id','payments_id']);
			$table->index('payments_id','fk_payments_informations_payments1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments_informations');
	}

}
