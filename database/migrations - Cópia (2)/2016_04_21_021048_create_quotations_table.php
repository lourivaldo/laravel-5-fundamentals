<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuotationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('quotations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->float('rate_starting', 10, 0);
			$table->float('rate_back', 10, 0)->nullable()->default(0);
			$table->dateTime('date_time_arrival_starting')->nullable();
			$table->dateTime('date_time_arrival_back')->nullable();
			$table->dateTime('date_time_output_starting')->nullable();
			$table->dateTime('date_time_output_back')->nullable();
			$table->string('company_starting', 10);
			$table->string('company_back', 10)->nullable();
			$table->float('price_quotation_starting', 10, 0);
			$table->float('price_quotation_back', 10, 0)->nullable();
			$table->float('economy_starting', 10, 0);
			$table->float('economy_back', 10, 0)->nullable();
			$table->string('flight_starting', 20);
			$table->string('flight_back', 20)->nullable();
			$table->text('observations', 65535)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('searchs_id')->index('fk_quotations_searchs1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('quotations');
	}

}
