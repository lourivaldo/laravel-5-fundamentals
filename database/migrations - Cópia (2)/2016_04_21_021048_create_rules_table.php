<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rules', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('manager')->default(0);
			$table->boolean('edit')->default(0);
			$table->boolean('update')->default(0);
			$table->boolean('create')->default(0);
			$table->boolean('store')->default(0);
			$table->boolean('show')->default(0);
			$table->boolean('index')->default(0);
			$table->boolean('destroy')->default(0);
			$table->boolean('trash')->default(0);
			$table->boolean('restore')->default(0);
			$table->boolean('quotation')->default(0);
			$table->boolean('users')->default(0);
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('roles_id')->index('fk_rules_roles1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rules');
	}

}
