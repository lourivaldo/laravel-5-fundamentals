<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSearchsChildrenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('searchs_children', function(Blueprint $table)
		{
			$table->integer('searchs_id')->index('fk_searchs_has_children_searchs1_idx');
			$table->integer('children_id')->index('fk_searchs_has_children_children1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('searchs_children');
	}

}
