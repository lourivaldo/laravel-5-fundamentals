<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersMarkupsConfigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_markups_configs', function(Blueprint $table)
		{
			$table->foreign('markups_configs_id', 'fk_users_has_markups_configs_markups_configs1')->references('id')->on('markups_configs')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('users_id', 'fk_users_has_markups_configs_users1')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_markups_configs', function(Blueprint $table)
		{
			$table->dropForeign('fk_users_has_markups_configs_markups_configs1');
			$table->dropForeign('fk_users_has_markups_configs_users1');
		});
	}

}
