<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAgenciesMarkupsConfigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('agencies_markups_configs', function(Blueprint $table)
		{
			$table->foreign('agencies_id', 'fk_agencies_has_markups_configs_agencies1')->references('id')->on('agencies')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('markups_configs_id', 'fk_agencies_has_markups_configs_markups_configs1')->references('id')->on('markups_configs')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('agencies_markups_configs', function(Blueprint $table)
		{
			$table->dropForeign('fk_agencies_has_markups_configs_agencies1');
			$table->dropForeign('fk_agencies_has_markups_configs_markups_configs1');
		});
	}

}
