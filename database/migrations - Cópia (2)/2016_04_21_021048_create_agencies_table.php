<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgenciesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('agencies', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title', 125);
			$table->boolean('mailer')->default(0);
			$table->string('cpf_cnpj', 25)->unique('cnpj_UNIQUE');
			$table->string('image', 225)->nullable();
			$table->string('email', 125)->unique('email_UNIQUE');
			$table->bigInteger('phone');
			$table->boolean('code')->default(81);
			$table->string('site', 300)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('agencies');
	}

}
