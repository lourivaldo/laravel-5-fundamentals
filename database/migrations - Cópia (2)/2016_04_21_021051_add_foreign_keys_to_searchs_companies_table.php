<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSearchsCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('searchs_companies', function(Blueprint $table)
		{
			$table->foreign('companies_id', 'fk_searchs_has_companies_companies1')->references('id')->on('companies')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('searchs_id', 'fk_searchs_has_companies_searchs1')->references('id')->on('searchs')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('searchs_companies', function(Blueprint $table)
		{
			$table->dropForeign('fk_searchs_has_companies_companies1');
			$table->dropForeign('fk_searchs_has_companies_searchs1');
		});
	}

}
