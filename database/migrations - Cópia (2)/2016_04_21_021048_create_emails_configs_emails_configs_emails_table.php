<?php

use Illuminate\Database\Migrations\Migration;

class CreateEmailsConfigsEmailsConfigsEmailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('emails_configs_emails_configs_emails', function(\database\Blueprint $table)
		{
			$table->integer('emails_configs_id');
			$table->integer('emails_configs_emails_id');
			$table->primary(['emails_configs_id','emails_configs_emails_id']);
			$table->index('emails_configs_id','fk_e_c_has_e_c1_idx');
			$table->index('emails_configs_emails_id','fk_e_c_has_e_c__idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('emails_configs_emails_configs_emails');
	}

}
