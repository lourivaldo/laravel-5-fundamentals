<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSearchsPassengersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('searchs_passengers', function(Blueprint $table)
		{
			$table->integer('searchs_id')->index('fk_searchs_has_passengers_searchs1_idx');
			$table->integer('passengers_id')->index('fk_searchs_has_passengers_passengers1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('searchs_passengers');
	}

}
