<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSpecificRulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('specific_rules', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('routes', 65535);
			$table->text('controllers', 65535);
			$table->text('actions', 65535);
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('rules_id')->index('fk_especific_rules_rules1_idx');
			$table->text('modules', 65535);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('specific_rules');
	}

}
