<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExceptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('exceptions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->float('miles', 10, 0);
			$table->float('price', 10, 0);
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('markups_id')->index('fk_exceptions_markups1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('exceptions');
	}

}
