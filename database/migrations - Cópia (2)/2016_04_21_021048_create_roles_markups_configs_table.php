<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRolesMarkupsConfigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('roles_markups_configs', function(Blueprint $table)
		{
			$table->integer('markups_configs_id')->index('fk_roles_has_markups_configs_markups_configs1_idx');
			$table->integer('roles_id')->index('fk_roles_has_markups_configs_roles1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('roles_markups_configs');
	}

}
