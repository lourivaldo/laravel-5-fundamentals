<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIntervalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('intervals', function(Blueprint $table)
		{
			$table->foreign('markups_id', 'fk_intervals_markups1')->references('id')->on('markups')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('intervals', function(Blueprint $table)
		{
			$table->dropForeign('fk_intervals_markups1');
		});
	}

}
