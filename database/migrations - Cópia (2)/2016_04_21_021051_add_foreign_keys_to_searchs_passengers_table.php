<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSearchsPassengersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('searchs_passengers', function(Blueprint $table)
		{
			$table->foreign('passengers_id', 'fk_searchs_has_passengers_passengers1')->references('id')->on('passengers')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('searchs_id', 'fk_searchs_has_passengers_searchs1')->references('id')->on('searchs')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('searchs_passengers', function(Blueprint $table)
		{
			$table->dropForeign('fk_searchs_has_passengers_passengers1');
			$table->dropForeign('fk_searchs_has_passengers_searchs1');
		});
	}

}
