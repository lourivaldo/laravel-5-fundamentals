<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailsConfigsEmailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('emails_configs_emails', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('email', 125)->unique('email_UNIQUE');
			$table->boolean('cc')->default(0);
			$table->boolean('cco')->default(0);
			$table->boolean('reply')->default(0);
			$table->boolean('default')->default(0);
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('emails_configs_emails');
	}

}
