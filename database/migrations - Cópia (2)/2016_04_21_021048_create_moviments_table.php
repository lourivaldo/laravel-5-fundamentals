<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMovimentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('moviments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cards_id')->index('fk_moviments_cards1_idx');
			$table->float('miles', 10, 0);
			$table->boolean('type');
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('stocks_id')->index('fk_moviments_stocks1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('moviments');
	}

}
