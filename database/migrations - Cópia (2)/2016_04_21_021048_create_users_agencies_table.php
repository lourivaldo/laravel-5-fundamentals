<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersAgenciesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_agencies', function(Blueprint $table)
		{
			$table->integer('users_id')->index('fk_users_has_agencies_users1_idx');
			$table->integer('agencies_id')->index('fk_users_has_agencies_agencies1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_agencies');
	}

}
