<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMarkupsConfigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('markups_configs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->float('percent', 10, 0);
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('markups_id')->unique("markups_id_UNIQUE")->index('fk_markups_configs_markups1_idx');
//			$table->primary(['id','markups_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('markups_configs');
	}

}
