<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfigsRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('configs_roles', function(Blueprint $table)
		{
			$table->integer('configs_id')->index('fk_configs_has_roles_configs1_idx');
			$table->integer('roles_id')->index('fk_configs_has_roles_roles1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('configs_roles');
	}

}
