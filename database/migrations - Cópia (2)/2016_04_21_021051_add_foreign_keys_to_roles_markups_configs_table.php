<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToRolesMarkupsConfigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('roles_markups_configs', function(Blueprint $table)
		{
			$table->foreign('markups_configs_id', 'fk_roles_has_markups_configs_markups_configs1')->references('id')->on('markups_configs')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('roles_id', 'fk_roles_has_markups_configs_roles1')->references('id')->on('roles')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('roles_markups_configs', function(Blueprint $table)
		{
			$table->dropForeign('fk_roles_has_markups_configs_markups_configs1');
			$table->dropForeign('fk_roles_has_markups_configs_roles1');
		});
	}

}
