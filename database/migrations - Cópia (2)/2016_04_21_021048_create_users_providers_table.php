<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_providers', function(Blueprint $table)
		{
			$table->integer('users_id')->index('fk_users_has_providers_users1_idx');
			$table->integer('providers_id')->index('fk_users_has_providers_providers1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_providers');
	}

}
