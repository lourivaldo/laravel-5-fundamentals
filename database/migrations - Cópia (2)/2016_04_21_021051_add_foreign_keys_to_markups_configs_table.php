<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMarkupsConfigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('markups_configs', function(Blueprint $table)
		{
			$table->foreign('markups_id', 'fk_markups_configs_markups1')->references('id')->on('markups')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('markups_configs', function(Blueprint $table)
		{
			$table->dropForeign('fk_markups_configs_markups1');
		});
	}

}
