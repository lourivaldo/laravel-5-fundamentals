<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAirportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('airports', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('title', 225);
			$table->string('subtitle', 225)->nullable();
			$table->string('country', 30)->default('Brasil');
			$table->char('uf', 5);
			$table->string('city', 125);
			$table->char('airport_initial', 4);
			$table->char('initials', 4)->unique('initials_UNIQUE');
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('airports');
	}

}
