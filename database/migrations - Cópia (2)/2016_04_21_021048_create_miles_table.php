<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('miles', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->float('miles', 10, 0);
			$table->boolean('type_trip');
			$table->string('type_mile', 20);
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('quotations_id')->index('fk_miles_quotations1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('miles');
	}

}
