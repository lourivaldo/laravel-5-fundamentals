<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBehaviorsRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('behaviors_roles', function(Blueprint $table)
		{
			$table->foreign('behaviors_id', 'fk_behaviors_has_roles_behaviors1')->references('id')->on('behaviors')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('roles_id', 'fk_behaviors_has_roles_roles1')->references('id')->on('roles')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('behaviors_roles', function(Blueprint $table)
		{
			$table->dropForeign('fk_behaviors_has_roles_behaviors1');
			$table->dropForeign('fk_behaviors_has_roles_roles1');
		});
	}

}
