<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProvidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('providers', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 125);
			$table->string('cpf_cnpj', 30)->unique('cnpj_UNIQUE');
			$table->string('image', 225)->nullable();
			$table->string('email', 125)->unique('email_UNIQUE');
			$table->bigInteger('phone');
			$table->boolean('code');
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('providers');
	}

}
