<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccessesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('accesses', function(Blueprint $table)
		{
			$table->integer('id')->primary();
			$table->string('address', 60);
			$table->string('browser');
			$table->string('device', 45);
			$table->string('mac_address', 45)->unique('mac_address_UNIQUE');
			$table->string('ip', 20);
			$table->string('operating_system', 60);
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('users_id')->nullable()->index('fk_accesses_users1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('accesses');
	}

}
