<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSearchsChildrenTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('searchs_children', function(Blueprint $table)
		{
			$table->foreign('children_id', 'fk_searchs_has_children_children1')->references('id')->on('children')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('searchs_id', 'fk_searchs_has_children_searchs1')->references('id')->on('searchs')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('searchs_children', function(Blueprint $table)
		{
			$table->dropForeign('fk_searchs_has_children_children1');
			$table->dropForeign('fk_searchs_has_children_searchs1');
		});
	}

}
