<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBehaviorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('behaviors', function(Blueprint $table)
		{
			$table->foreign('roles_id', 'fk_behaviors_roles1')->references('id')->on('roles')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('behaviors', function(Blueprint $table)
		{
			$table->dropForeign('fk_behaviors_roles1');
		});
	}

}
