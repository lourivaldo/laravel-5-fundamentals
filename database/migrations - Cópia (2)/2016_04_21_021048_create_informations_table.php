<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInformationsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informations', function (Blueprint $table) {
            $table->integer('id');
            $table->string('avatar', 225)->nullable()->default('default.png');
            $table->string('cpf_cnpj', 30)->nullable()->unique('cpf_cnpj_UNIQUE');
            $table->bigInteger('rg')->nullable()->unique('rg_UNIQUE');
            $table->integer('code')->default(81);
            $table->bigInteger('phone')->nullable();
            $table->date('birthday')->nullable();
            $table->text('addtional', 65535)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('status')->default(1);
            $table->integer('users_id');
            $table->primary(['id', 'users_id']);
            $table->index('users_id','fk_informations_users1_idx');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('informations');
    }

}
