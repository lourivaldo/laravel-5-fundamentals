<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSpecificRulesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('specific_rules', function(Blueprint $table)
		{
			$table->foreign('rules_id', 'fk_especific_rules_rules1')->references('id')->on('rules')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('specific_rules', function(Blueprint $table)
		{
			$table->dropForeign('fk_especific_rules_rules1');
		});
	}

}
