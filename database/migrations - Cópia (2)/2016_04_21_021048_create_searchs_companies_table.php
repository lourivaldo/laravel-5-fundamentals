<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSearchsCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('searchs_companies', function(Blueprint $table)
		{
			$table->integer('searchs_id')->index('fk_searchs_has_companies_searchs1_idx');
			$table->integer('companies_id')->index('fk_searchs_has_companies_companies1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('searchs_companies');
	}

}
