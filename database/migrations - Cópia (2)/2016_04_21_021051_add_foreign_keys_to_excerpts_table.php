<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExcerptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('excerpts', function(Blueprint $table)
		{
			$table->foreign('quotations_id', 'fk_excerpts_quotations1')->references('id')->on('quotations')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('excerpts', function(Blueprint $table)
		{
			$table->dropForeign('fk_excerpts_quotations1');
		});
	}

}
