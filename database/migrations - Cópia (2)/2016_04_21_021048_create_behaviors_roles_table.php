<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBehaviorsRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('behaviors_roles', function(Blueprint $table)
		{
			$table->integer('behaviors_id')->index('fk_behaviors_has_roles_behaviors1_idx');
			$table->integer('roles_id')->index('fk_behaviors_has_roles_roles1_idx');
			$table->unique(['behaviors_id','roles_id'], 'composite_key');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('behaviors_roles');
	}

}
