<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTicketsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tickets', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->bigInteger('sale_number');
			$table->string('tracker', 30)->unique('tracker_UNIQUE');
			$table->string('ticket_number', 30)->nullable()->unique('ticket_number_UNIQUE');
			$table->string('provider', 30);
			$table->text('observations', 65535)->nullable();
			$table->bigInteger('credit_card');
			$table->boolean('payment_form');
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('quotations_id')->index('fk_tickets_quotations1_idx');
			$table->integer('users_id')->index('fk_tickets_users1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tickets');
	}

}
