<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExcerptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('excerpts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->boolean('type_trip')->default(0);
			$table->string('flight', 12)->nullable();
			$table->char('origin', 5);
			$table->char('arrival', 5);
			$table->time('time_starting');
			$table->time('time_arrival');
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('quotations_id')->index('fk_excerpts_quotations1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('excerpts');
	}

}
