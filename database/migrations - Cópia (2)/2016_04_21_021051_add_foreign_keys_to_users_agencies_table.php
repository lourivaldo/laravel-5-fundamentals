<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUsersAgenciesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users_agencies', function(Blueprint $table)
		{
			$table->foreign('agencies_id', 'fk_users_has_agencies_agencies1')->references('id')->on('agencies')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('users_id', 'fk_users_has_agencies_users1')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users_agencies', function(Blueprint $table)
		{
			$table->dropForeign('fk_users_has_agencies_agencies1');
			$table->dropForeign('fk_users_has_agencies_users1');
		});
	}

}
