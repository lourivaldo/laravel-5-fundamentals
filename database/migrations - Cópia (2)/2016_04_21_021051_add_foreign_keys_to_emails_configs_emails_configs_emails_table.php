<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToEmailsConfigsEmailsConfigsEmailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('emails_configs_emails_configs_emails', function(Blueprint $table)
		{
			$table->foreign('emails_configs_id', 'fk_e_c_has_e_c1')->references('id')->on('emails_configs')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('emails_configs_emails_id', 'fk_e_c_has_e_c_em1')->references('id')->on('emails_configs_emails')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('emails_configs_emails_configs_emails', function(Blueprint $table)
		{
			$table->dropForeign('fk_e_c_has_e_c1');
			$table->dropForeign('fk_e_c_has_e_c_em1');
		});
	}

}
