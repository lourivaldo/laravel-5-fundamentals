<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfigsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('configs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('apis_id')->index('fk_configs_apis1_idx');
			$table->integer('search_limit');
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->string('display_name', 125)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('configs');
	}

}
