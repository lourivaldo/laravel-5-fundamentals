<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOpsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ops', function(Blueprint $table)
		{
			$table->integer('id');
			$table->string('title', 225);
			$table->float('final_price', 10, 0);
			$table->float('discount', 10, 0)->default(0);
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('quotations_id');
			$table->primary(['id','quotations_id']);
			$table->index('quotations_id','fk_ops_quotations1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ops');
	}

}
