<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPaymentsInformationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('payments_informations', function(Blueprint $table)
		{
			$table->foreign('payments_id', 'fk_payments_informations_payments1')->references('id')->on('payments')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('payments_informations', function(Blueprint $table)
		{
			$table->dropForeign('fk_payments_informations_payments1');
		});
	}

}
