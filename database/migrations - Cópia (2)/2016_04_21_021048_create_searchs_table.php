<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSearchsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('searchs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->date('date_starting');
			$table->date('date_back')->nullable();
			$table->char('origin', 4);
			$table->char('destination', 4);
			$table->boolean('type_trip');
			$table->integer('adults');
			$table->integer('children');
			$table->integer('babies')->default(0);
			$table->boolean('user_is_passenger')->default(1);
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
			$table->integer('users_id')->index('fk_searchs_users1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('searchs');
	}

}
