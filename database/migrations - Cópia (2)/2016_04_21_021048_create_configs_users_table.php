<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfigsUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('configs_users', function(Blueprint $table)
		{
			$table->integer('configs_id')->index('fk_configs_has_users_configs1_idx');
			$table->integer('users_id')->index('fk_configs_has_users_users1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('configs_users');
	}

}
