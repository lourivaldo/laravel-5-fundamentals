<!DOCTYPE html>
<html ng-app="laravelApp">
<head>
    <title>Laravel</title>
    <link href="/css/bootstrap.min.css">
    {{--<link href="/plugins/datatables/css/jquery.datatables.min.css">--}}
    <link href="/plugins/angular-datatables/css/angular-datatables.min.css">
</head>
<body>
<div class="container">
    <div class="content" ng-controller="WithAjaxCtrl as showCase">
        <div class="row">
            <div class="col-md-12">
                <table datatable="" dt-options="showCase.dtOptions" dt-columns="showCase.dtColumns" class="row-border hover"></table>
				<uib-pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm" boundary-links="true" num-pages="numPages"></uib-pagination>
			</div>
        </div>
    </div>
</div>
<script src="/js/jquery.min.js"></script>
<script src="/plugins/datatables/js/jquery.datatables.min.js"></script>
<script src="/plugins/angular/angular.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-animate.js"></script>
<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-1.3.3.js"></script>
<script src="/plugins/angular-datatables/angular-datatables.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/locale/pt-br.js"></script>

<script src="/js/bootstrap/bootstrap.min.js"></script>

<script src="/js/user/index.js"></script>
</body>
</html>
