<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
    <link href="/css/bootstrap.min.css">
    <link href="/css/bootstrap-datetimepicker.css">
</head>
<body>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div id="container-chart-2"></div>
            </div>
            {{--<div class="row">
                <div class="col-md-6">
                    <ul id="api-sum-selected" class="list-inline list-unstyled"></ul>
                </div>
            </div>--}}
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="container-chart"></div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <ul id="api-sum-selected" class="list-inline list-unstyled"></ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/locale/pt-br.js"></script>

<script src="/js/bootstrap/bootstrap.min.js"></script>
<script src="/js/bootstrap/transition.js"></script>
<script src="/js/bootstrap/collapse.js"></script>
<script src="/js/bootstrap/bootstrap-datetimepicker.js"></script>

<script src="/js/highcharts/stock/highstock.js"></script>
<script src="/js/highcharts/stock/modules/exporting.js"></script>
<script src="/js/highcharts/locale/pt-br.js"></script>
<script src="/js/charts/index.js"></script>
<script src="/js/charts/chart-2.js"></script>
<script src="/js/charts/theme.js"></script>
</body>
</html>
