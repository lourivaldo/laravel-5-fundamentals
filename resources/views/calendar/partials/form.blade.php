<div class="row">
    @if(isset($errors))
        @foreach($errors->all() as $error)
            {!! $error !!}
        @endforeach
    @endif

    <div class="col-sm-12 col-md-6">
        <div class="panel panel-white">
            <div class="panel-heading clearfix">
                <h4 class="panel-title">Evento</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            {!! Form::hidden('apis') !!}
                            @foreach($apis as $api)
                                {!! Form::checkbox('apis[]', $api->id, isset($data)? in_array($api->id, $data->apis()->select('id')->lists('id')->toArray()) : null, ['class'=>'', 'id'=>"apis-$api->id"]) !!}
                                {!! Form::label("apis-$api->id", $api->name) !!}
                            @endforeach
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12">
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class='input-group date datetime-picker----'>
                                    {!! Form::text('start', null, ['id'=>'start','class'=>'form-control']) !!}
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-2">
                                {!! Form::label('start','até') !!}
                            </div>
                            <div class="col-sm-12 col-md-5">
                                <div class='input-group date datetime-picker----'>
                                    {!! Form::text('end',null,['id'=>'end','class'=>'form-control']) !!}
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            {!! Form::label('color','Cor') !!}
                            {!! Form::color('color', null,['class'=>'form-control']) !!}
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            {!! Form::hidden('all_day',0) !!}
                            {!! Form::checkbox('all_day',1,null) !!}
                            {!! Form::label('all_day','Dia inteiro') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        {!! Form::hidden('status',0) !!}
                        <div class="form-group">
                            {!! Form::checkbox('status',1,1) !!}
                            {!! Form::label('status','Ativar') !!}
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            <a href="{{ route('manager.calendar.index') }}" class="btn btn-primary">Cancelar</a>
                            {!! Form::submit("Salvar", ['id'=>'save','class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>

    <div class="col-sm-12 col-md-6">
        @include('calendar.partials.form-repeat')
    </div>

</div>



