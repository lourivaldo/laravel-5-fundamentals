<div class="panel panel-white">
    <div class="panel-heading clearfix">
        <h4 class="panel-title">
            {!! Form::checkbox('repeatable',1,(isset($data) && is_null($data->repeat) ? 0 : 1)) !!} {!! Form::label('repeat','Repetir') !!}
        </h4>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="form-group">
                    {!! Form::label('repeat[repeats_types_id]','Repetição:') !!}
                    {!! Form::select('repeat[repeats_types_id]',$repeatsTypes,null,['class'=>'form-control', 'id'=>'repeat-type','style'=>'width:inherit;display:inline;display:inline']) !!}
                </div>

                <div class="form-group" id="repeat-target-2" style="display: none;">
                    {{--Dia da semana--}}
                    {{--@Todo:check for dow from start date--}}
                    {!! Form::label('repeat[dows][]','Repete:') !!}
                    {!! Form::checkbox('repeat[dows][]', 0, null, ['id'=>'dows-0']) !!}{!! Form::label('dows-0','D') !!}
                    {!! Form::checkbox('repeat[dows][]', 1, null, ['id'=>'dows-1']) !!}{!! Form::label('dows-1','S') !!}
                    {!! Form::checkbox('repeat[dows][]', 2, null, ['id'=>'dows-2']) !!}{!! Form::label('dows-2','T') !!}
                    {!! Form::checkbox('repeat[dows][]', 3, null, ['id'=>'dows-3']) !!}{!! Form::label('dows-3','Q') !!}
                    {!! Form::checkbox('repeat[dows][]', 4, null, ['id'=>'dows-4']) !!}{!! Form::label('dows-4','Q') !!}
                    {!! Form::checkbox('repeat[dows][]', 5, null, ['id'=>'dows-5']) !!}{!! Form::label('dows-5','S') !!}
                    {!! Form::checkbox('repeat[dows][]', 6, null, ['id'=>'dows-6']) !!}{!! Form::label('dows-6','S') !!}
                </div>

                <div class="form-group" id="repeat-target-3" style="display: none;">
                    {{--Dia do mes--}}
                    {!! Form::label('repeat[dom]','Repetir:') !!}
                    {!! Form::radio('repeat[dom]', 0, null, ['id'=>'dom-0']) !!}{!! Form::label('dom-1','dia da semana') !!}
                    {!! Form::radio('repeat[dom]', 1, null, ['id'=>'dom-1']) !!}{!! Form::label('dom-0','dia do mês') !!}
                </div>

                <hr>
                <div>
                    <strong>Termina:</strong>
                </div>

                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        {!! Form::radio('repeat[end_type]',REPEAT_TYPE_EVER,1,['class'=>'form-control']) !!}
                        {!! Form::label('end_type','Nunca') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::radio('repeat[end_type]',REPEAT_TYPE_OCCURRENCES,null,['class'=>'form-control']) !!}
                        {!! Form::label('occurrences','Após:') !!}
                        {!! Form::number('repeat[occurrences]',null,['min'=>'0', 'max'=>'999999','class'=>'form-control','style'=>'width:inherit;display:inline']) !!}
                        {!! Form::label('occurrences','ocorrências') !!}
                    </div>

                    <div class="form-group">
                        {!! Form::radio('repeat[end_type]',REPEAT_TYPE_DATE,null,['class'=>'form-control','style'=>'width:inherit;display:inline']) !!}
                        {!! Form::label('repeat[end]','Em') !!}
                        {!! Form::text('repeat[end]',null,['class'=>'form-control','style'=>'width:inherit;display:inline']) !!}
                    </div>
                </div>

                {{--<div class="form-group">
                    <strong>Resumo:</strong><span>resumo aqui</span>
                </div>--}}
            </div>
        </div>

    </div>
</div>
