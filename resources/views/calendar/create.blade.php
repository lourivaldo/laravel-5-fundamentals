@extends("layouts.calendar")

@section('content')

    <div class="col-md-12">
        {!! Form::open(['route'=>'manager.calendar.store','id'=>'form-add-avent']) !!}
        @include('calendar.partials.form')
        {!! Form::close() !!}
    </div>
@stop