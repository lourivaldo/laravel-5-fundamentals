@extends("layouts.calendar")

@section('content')

    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h4 class="panel-title">Agenda API</h4>
                </div>
                <div class="panel-body">
                    <a href="{{ route('manager.calendar.create') }}" class="btn btn-success m-b-sm">Novo Evento</a>
                    <div id='calendar'></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4"></div>

@stop