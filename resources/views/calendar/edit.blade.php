@extends("layouts.calendar")

@section('content')

    <div class="col-md-12">
        {!! Form::model($data, ['method' => 'put', 'route'=>['manager.calendar.update',$data->id],'id'=>'form-add-avent']) !!}
        @include('calendar.partials.form')
        {!! Form::close() !!}
    </div>

@stop