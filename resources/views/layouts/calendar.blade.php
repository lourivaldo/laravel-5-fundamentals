<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="UTF-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">
    <link href='/plugins/fullcalendar/fullcalendar.css' rel='stylesheet' />
    <link href='/plugins/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
    <style>
        body {
            margin: 40px 10px;
            padding: 0;
            font-family: "Lucida Grande", Helvetica, Arial, Verdana, sans-serif;
            font-size: 14px;
        }

        #calendar {
            max-width: 900px;
            margin: 0 auto;
        }
    </style>
</head>

<body>

<div class="container">
    @section("content")
    @show
</div>

<script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/locale/pt-br.js"></script>
<script src="/js/bootstrap/transition.js"></script>
<script src="/js/bootstrap/collapse.js"></script>
<script src='/plugins/fullcalendar/fullcalendar.min.js'></script>
<script src='/plugins/fullcalendar/lang/pt-br.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ url("js/calendar/index.js") }}"></script>
<script src="{{ url("js/calendar/create.js") }}"></script>

</body>
</html>