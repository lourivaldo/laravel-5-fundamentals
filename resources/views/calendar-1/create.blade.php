@extends('layouts.calendar')
<div class="row">
    {!! Form::open(['route'=>'calendar.store']) !!}
    @include('calendar.partials.form')
    {!! Form::close() !!}
</div>