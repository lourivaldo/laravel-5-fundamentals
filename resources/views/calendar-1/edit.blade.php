@extends('layouts.calendar')
<div class="row">
    {!! Form::model($event, ['method' => 'put', 'route'=>['calendar.update',$event->id]]) !!}
    @include('calendar.partials.form')
    {!! Form::close() !!}
</div>