{!! Form::open(['route'=>'calendar.create']) !!}
<h2>Repetir</h2>
<div>
    {!! Form::label('api','API') !!}
    {!! Form::select('api',[0 => 'API 1', 1 => 'API 2', 3 => 'API 3(Não Ativa)']) !!}
</div>

<div>
    {!! Form::label('api','API') !!}
    {!! Form::checkbox('api',0,1) !!}{!! Form::label('api','API 1') !!}
    {!! Form::checkbox('api',1,1) !!}{!! Form::label('api','API 2') !!}
    {!! Form::checkbox('api',2,1) !!}{!! Form::label('api','API 3 (Não Ativa)') !!}
</div>
<br>
<div>
    {!! Form::label('frequency','Repetição:') !!}
    {!! Form::select('frequency',[0=>'Todos os dias',1 => 'Semanal:',2 => 'Mensal',3 => 'Anual']) !!}
</div>

<div>
    {!! Form::label('interval','Repete a cada:') !!}
    {!! Form::select('interval',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30]) !!}
    {!! Form::label('interval','dias') !!}
    {!! Form::label('interval','semanas') !!}
    {!! Form::label('interval','meses') !!}
    {!! Form::label('interval','anos') !!}
</div>

<div>
    {{--Dia da semana--}}
    {!! Form::label('dow','Repete:') !!}
    {!! Form::checkbox('dow', 0) !!}{!! Form::label('dow','D') !!}
    {!! Form::checkbox('dow', 1) !!}{!! Form::label('dow','S') !!}
    {!! Form::checkbox('dow', 2) !!}{!! Form::label('dow','T') !!}
    {!! Form::checkbox('dow', 3) !!}{!! Form::label('dow','Q') !!}
    {!! Form::checkbox('dow', 4) !!}{!! Form::label('dow','Q') !!}
    {!! Form::checkbox('dow', 5) !!}{!! Form::label('dow','S') !!}
    {!! Form::checkbox('dow', 6) !!}{!! Form::label('dow','S') !!}
</div>

<div>
    {{--Dia do mes--}}
    {!! Form::label('dom','Repetir:') !!}
    {!! Form::radio('dom', 0) !!}{!! Form::label('dom','dia do mês') !!}
    {!! Form::radio('dom', 1) !!}{!! Form::label('dom','dia da semana') !!}
</div>

<div>
    {!! Form::label('start','Início em:') !!}
    {!! Form::date('start') !!}
</div>

<div>
    {!! Form::label('end','Termina:') !!}

    <div>
        {!! Form::radio('end',0,1,['id'=>'end-never']) !!}
        {!! Form::label('end-never','Sempre') !!}
    </div>
    <div>
        {!! Form::radio('end',1,null,['id'=>'end-count']) !!}
        {!! Form::label('end-count','Após:') !!}
        {!! Form::number('end_count',1,['id'=>'end-count', 'min'=>'1', 'max'=>'999999']) !!}
        {!! Form::label('end-count','ocorrências') !!}
    </div>
    <div>
        {!! Form::radio('end',2,null,['id'=>'end-until']) !!}
        {!! Form::label('end-until','Em') !!}
        {!! Form::date('end') !!}
    </div>
</div>

<div>
    {!! Form::label('summary','Resumo:') !!}
    {!! Form::label('summary','resumo aqui') !!}
</div>
{!! Form::close() !!}

<script>

</script>
