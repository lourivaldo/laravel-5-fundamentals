<div class="row">
    <div class="col-sm-12 col-md-12">
        <h2>Repetir</h2>
        @if(isset($errors))
            @foreach($errors->all() as $error)
                {!! $error !!}
            @endforeach
        @endif
        <div class="col-sm-4 col-md-4">
            {!! Form::hidden('apis') !!}
            @foreach($apis as $api)
                <div class="col-sm-12 col-md-12">
                    {!! Form::checkbox('apis[]', $api->id, isset($event)? in_array($api->id, $event->apis()->select('id')->lists('id')->toArray()) : null, ['class'=>'', 'id'=>"apis-$api->id"]) !!}
                    {!! Form::label("apis-$api->id", $api->name) !!}
                </div>
            @endforeach
        </div>
    </div>

    <div class="col-sm-12 col-md-12">
        <div class="row">
            <div class="col-sm-3 col-md-3">
                <div class='input-group date datetime-picker'>
                    {!! Form::text('start', null, ['class'=>'form-control']) !!}
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
            <div class="col-sm-1 col-md-1">
                {!! Form::label('start','até') !!}
            </div>
            <div class="col-sm-3 col-md-3">
                <div>
                    <div class='input-group date datetime-picker'>
                        {!! Form::text('end',null,['class'=>'form-control']) !!}
                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-12">
        <div>
            {!! Form::checkbox('all_day') !!}
            {!! Form::label('all_day','dia inteiro') !!}
            {!! Form::hidden('all_day') !!}
            {!! Form::checkbox('repeat') !!}
            {!! Form::label('repeat','Repetir...') !!}
        </div>
    </div>
    {!! Form::submit() !!}

</div>
